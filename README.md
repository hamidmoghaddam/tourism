## Problem Statement
Our client operates in the Tourism industry and focused on reviving domestic tourism in New Zealand in a post COVID – 19 world. The client is interested in monitoring movement of domestic tourism from various parts of New Zealand to identify investment opportunities. Also, the client is working with out-of-home media agencies to place billboards that will attract tourists from strategic locations.

## Question One

Based on the transaction data structure in Page – 4, please illustrate an approach to estimate customers’ residential location i.e. point of origin, tourism destination and preferred mode of travel.

In order to answer this question, I assumed that the transaction data has been recorded in both origin and destination for each customer. In addition, I assumed that the data has not recorded in a round trip. Therefore, the first and last record of data for each customer show the origin and destination of the customer, respectively. It is possible to get the address of origin and destination for each customer by reverse geocoding of latitude and longitude of customers’ data. Limitations of this method are: a) There are some online transaction which not include any latitude and longitude; and b) This method does not consider multi-stop trips.
In order to calculate the mode of travel, I assumed that people use air travel if traveling between origin and destination require more than 6 hours driving by their car. The reason behind this idea is the cost of air traveling would be less than the cost of driving with a personal car for a distance with 6 hours driving. In 600 Km, cars in NZ needs 48.86 L (7.6 L/100 Km) of petrol in average which cost almost 100 NZD (Petrol price is 2.12). Limitations of this method are: a) The availability of airports should be checked in both origin and destination; b) The cost of traveling might not be a valid reason for travel mode selection for all customer; and c) This method does not consider other type of public transports.

## Question Two
How would you determine hot spots that are ideal for tourism investments ? Please feel free to add any data available in the public domain to augment with the data available in Page – 4. Please illustrate a methodology that can best support your approach.

In order to answer this question, I assumed that the spots (e.g, cities) that customers spend more money would be hot spots. Therefore, the hot spots can be determined by grouping the data by address that are extracted for question one, in different level (e.g., regions, cities and suburbs). It is also possible to find the hot spots for different categories by this method. Limitations of this method are: a) The online transactions can not be considered; and b) The data might not be sufficient for forecasting future which is necessary for investments.
In order to solve the forecasting problem, I collected an additional data provided by NZ government [1] and applied a Ridge regression to forecast the future hot spots.Limitations of this method are: a) COVID-19 might effect the future forecast; and b) The data is in high level (region) so getting lower level hot spots is not possible.

## Question Three
How would you determine the ideal locations for billboard placements to attract tourists when customers are on the move (i.e. away from residence)?

In order to answer this question, I assumed that the billboard placement should be close to the destination of customers. Therefore, I found the most frequent road used by customers and then search for nearby petrol stations for installing the billboard. Limitations of this method are: a) The profit of installing billboards is not calculated; b) Only petrol station is considered as the potential place for billboard; and c) the available billboard in the area
is not considered.

## References
[1] N. Government, Monthly Regional Tourism Estimates, 2021. https://www.mbie.govt.nz/immigration-and-tourism/ tourism-research-and-data/tourism-data-releases/ monthly-regional-tourism-estimates/latest-update/data-download/.

## How to run

First install all required libraries:

`pip install -r requirements.txt`

Then run the dashboard by:

`streamlit run main.py`
