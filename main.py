import streamlit as st
import pandas as pd
import numpy as np
from geopy.geocoders import Nominatim
from utils import fit_transform,SearchCity, add_marker_map,init_map,get_map_center_zoom,find_short_route,heat_map
from utils import get_cafes_around_point,heat_map_childs


#  TomTom API key Setting
with open('apikey.txt') as f:
    api_key = f.readline()

# set page layout
st.set_page_config(
    page_title="Visa use case",
    page_icon="🌍",
    layout="wide",
    initial_sidebar_state="expanded",
)

# Implementing the sidebar
st.sidebar.subheader("Inputs")


def load_data():
    """
    Loading the data
    :return a pandas dataframe:
    """
    # Read the data
    df = pd.read_csv('./data/visa.csv')

    # Convert date and time columns to a datetime column
    df['date'] = pd.to_datetime(df['Txn_dt'].astype(str)+' '+df['Txn_time'].astype(str),format='%d%m%Y %H%M%S')

    # fix the issue of --
    df['Lat_long'] = df['Lat_long'].str.replace('--', '-')
    df[['lat', 'lon']] = df['Lat_long'].str.split(',', 1, expand=True)

    # Sort dataframe by datetime value
    df.sort_values(by=['date'])
    return df


def get_address(location):
    """
    Extract the city, suburb and road from a location
    :param location:
    :return city, suburb, road:
    """

    # Get the city name from address dict
    tmp_city = location.raw.get('address','NaN').get('city', 'NaN')
    # Sometimes the address has town instead of city
    if tmp_city == 'NaN':
        tmp_city = location.raw.get('address', 'NaN').get('town', 'NaN')

    # Get suburb name from the address
    tmp_suburb = location.raw.get('address','NaN').get('suburb','NaN')

    # Get the county if the suburb is not available
    if tmp_suburb == 'NaN':
        tmp_suburb = location.raw.get('address', 'NaN').get('county', 'NaN')

    # Get the road name
    tmp_road = location.raw.get('address','NaN').get('road','NaN')
    return tmp_city, tmp_suburb, tmp_road


def geodecode(lat_lons):
    """
    Decoding lat and lon to the actual address
    :param lat_lons:
    :return city, suburb and road:
    """

    # Implement the Nominatim package
    locator = Nominatim(user_agent='myGeocoder', timeout=5)
    final_lst = []

    # Iterates over the locations
    for lat_lon in lat_lons:

        # Get the address
        location = locator.reverse(lat_lon)

        # Extract the city, suburb and road name from address
        final_lst.append(get_address(location))
    return final_lst


def generate_forecast_heatmap():
    """
    Generates a heat map for the forecast of 2021
    :return :
    """

    # Load additional date from https://www.mbie.govt.nz/immigration-and-tourism/tourism-research-and-data/
    # tourism-data-releases/monthly-regional-tourism-estimates/latest-update/data-download/

    df_ad = pd.read_csv('./data/additional_data.csv')

    # Convert date column to datetime
    df_ad['Date'] = pd.to_datetime(df_ad['Date'])

    # Extract the year of datetime column
    df_ad['year'] = df_ad['Date'].dt.year

    # Group by regions and year
    df_g = df_ad.groupby(['REGION', 'year'])['Spend'].sum().reset_index()

    # Get the name of regions
    regions = df_g['REGION'].unique()
    tmp_lst = []
    tmp_nxt_year = []

    # iterates over the regions
    for region in regions:

        # Filter the region dataframe
        filtered_df = df_g[df_g['REGION'] == region]

        # Train ridge regression and make forecast
        res = fit_transform(filtered_df['year'].values.reshape(-1, 1), filtered_df['Spend'].values, [[2021]])

        # Append results to a list
        tmp_nxt_year.append(res.tolist()[0])

    # Calculate the total spend in 2021
    sum_lst = sum(tmp_nxt_year)

    # Iterate over regions
    for i, region in enumerate(regions):

        # Create a dict
        tmp_dict = {'name': region}

        # Calculate percentage of spend for each region
        tmp_dict['weight'] = int((tmp_nxt_year[i] / sum_lst) * 100)

        # Find a lat and lon of region
        geo_id, position = SearchCity(region, 'New Zealand')

        # Add the lat and lon into the dict
        tmp_dict['lat'] = position['lat']
        tmp_dict['lon'] = position['lon']

        # Add a color for marker
        tmp_dict['color'] = 'red'

        # Append the dict into a list
        tmp_lst.append(tmp_dict)
        # st.write(geo_id, position)

    # Generate heatmap
    map2_ = heat_map(api_key, tmp_lst, zoom=5)

    # Add heatmap to streamlit
    map_box = st.markdown(map2_._repr_html_(), unsafe_allow_html=True)

def generate_map_customer(customer_data):
    """
    Generates a route map for a customer
    :param customer_data:
    :return map:
    """
    # Get home and destination location
    lat_home = customer_data.head(1).reset_index().loc[0, 'lat']
    long_home = customer_data.head(1).reset_index().loc[0, 'lon']
    lat_dest = customer_data.tail(1).reset_index().loc[0, 'lat']
    long_dest = customer_data.tail(1).reset_index().loc[0, 'lon']

    # TODO: Check for all none possibilities

    if (lat_home is not np.nan) and (lat_dest is not np.nan):

        # Calculate the center of map
        lat_avg, long_avg, zoom_avg = get_map_center_zoom(lat_home, long_home, lat_dest, long_dest)

        # Init the map
        map_ = init_map(api_key=api_key, latitude=lat_avg, longitude=long_avg, zoom=zoom_avg, layer="hybrid")

        # Add home marker to map
        map_ = add_marker_map(map_, lat_home, long_home, 'Home', 'green', is_home=True)

        # Add destination marker to map
        map_ = add_marker_map(map_, lat_dest, long_dest, 'Destination', 'red', is_home=False)

        # Finding short route between home and destination
        time_h, time_m, distance, eta_date, eta_time, map_, points = find_short_route(lat_home, long_home, lat_dest,
                                                                                      long_dest)

        # Get hours of trip
        if time_h > 6:
            st.write('Travel mode: Air')
        else:
            st.write('Travel mode: Car')

        # Add map to streamlit
        map_box = st.markdown(map_._repr_html_(), unsafe_allow_html=True)

    # Initialize map without home location
    elif lat_home is not np.nan:
        map_ = init_map(api_key=api_key, latitude=lat_home, longitude=long_home, zoom=14, layer="hybrid")

        map_ = add_marker_map(map_, lat_home, long_home, 'Home', 'green', is_home=True)
        map_box = st.markdown(map_._repr_html_(), unsafe_allow_html=True)

    # Initialize map without destination location
    elif lat_dest is not np.nan:
        map_ = init_map(api_key=api_key, latitude=lat_dest, longitude=long_dest, zoom=14, layer="hybrid")

        map_ = add_marker_map(map_, lat_dest, long_dest, 'Destination', 'red', is_home=False)
        map_box = st.markdown(map_._repr_html_(), unsafe_allow_html=True)
    else:
        st.error('There are not valid cordinations for the selected user')


def main():

    # Load the data
    df = load_data()

    # Remove the dollar sign
    df['Txn_amt'] = df['Txn_amt'].replace({'\$': ''}, regex=True).astype(int)

    # Create drop down
    selected_customer = st.sidebar.selectbox('Please select a customer', df['Cust_num'].unique())

    st.header("Q1: ")
    st.text("Based on the transaction data structure in Page – 4, please illustrate an approach to estimate customers’\n"
            "residential location i.e. point of origin, tourism destination and preferred mode of travel.")

    # Filter dataframe by selected customer
    customer_data = df[df['Cust_num'] == selected_customer]

    generate_map_customer(customer_data)

    st.header('Q2')
    st.text("How would you determine hot spots that are ideal for tourism investments ? Please feel free to add any data\n"
            " available in the public domain to augment with the data available in Page – 4. Please illustrate a methodology\n"
            " that can best support your approach")

    st.subheader('Spending based on provided data set')
    # Calculate the current share of each region
    df_g = df.groupby(['Cust_num'])

    # Get just destinations
    df_destination = (pd.concat([df_g.tail(1)]).drop_duplicates().sort_values('date').reset_index(drop=True))

    # Decode destinations
    tmp_lst = geodecode(df_destination['Lat_long'].values)
    stacked_tmp_lst = list(zip(*tmp_lst))
    df_destination['city'] = stacked_tmp_lst[0]
    df_destination['suburb'] = stacked_tmp_lst[1]
    df_destination['road'] = stacked_tmp_lst[2]

    map3_ = heat_map_childs(api_key,df_destination,zoom=8)
    map_box = st.markdown(map3_._repr_html_(), unsafe_allow_html=True)

    st.subheader('Forecast of spending in 2021')
    generate_forecast_heatmap()

    st.header('Q3')
    st.text("How would you determine the ideal locations for billboard placements to attract tourists when customers are\n"
            " on the move (i.e. away from residence) ?")

    # Group data by road and category and sum of spending
    df_g = df_destination.groupby(['road','M_cat'])['Txn_amt'].sum().reset_index()

    # Rename the column to sum
    df_g.rename(columns={'Txn_amt': 'sum'},inplace=True)

    # Merge the sum column into the original dataframe
    df_final = pd.merge(df_destination, df_g, on=['road','M_cat'], how='left')

    # Sort the dataframe by sum
    df_final.sort_values('sum', ascending=False, inplace=True)

    # Select the destination road
    selected_road = st.selectbox('Please select a destination place (road)', df_final['road'].unique())

    # Filter the data based on selected road
    df_filtered = df_final[df_final['road'] == selected_road]

    # find petrol station near the destination place
    # TODO: Make sure finding a location in same road
    res = get_cafes_around_point(df_filtered.reset_index().loc[0,'lat'],df_filtered.reset_index().loc[0,'lon'],
                                 100,500,'petrol station')
    final_lst = []

    # Extract the results
    for result in res['results']:
        final_lst.append([result['poi']['name'],result['address']['freeformAddress'],result['position']['lat'],
                          result['position']['lon']])

    # Print the results
    if len(final_lst) > 0:
        df_location = pd.DataFrame(final_lst, columns=['name','address','lat','lon'])
        st.write(df_location)
    else:
        st.error('Please try another category')


if __name__ == "__main__":
    main()
