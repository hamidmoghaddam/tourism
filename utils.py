from sklearn.linear_model import Ridge
import folium
from folium.plugins import MarkerCluster
import requests
from bs4 import BeautifulSoup

with open('apikey.txt') as f:
    api_key = f.readline()

def fit_transform(x_train,y_train,x_test):
    clf = Ridge()
    clf.fit(x_train,y_train)
    res = clf.predict(x_test)
    return res

def init_map(api_key=api_key, latitude=0, longitude=0, zoom=14, layer="basic", style="main"):
    """
    A function to initialize a clean TomTom map
    """

    maps_url = "http://{s}.api.tomtom.com/map/1/tile/" + layer + "/" + style + "/{z}/{x}/{y}.png?tileSize=512&key="
    TomTom_map = folium.Map(
        location=[latitude, longitude],  # on what coordinates [lat, lon] initialise the map
        zoom_start=zoom,
        tiles=str(maps_url + api_key),
        attr='TomTom')

    # add get latitude and longitude feature to our map
    folium.LatLngPopup().add_to(TomTom_map)

    return TomTom_map

def heat_map(api_key=api_key, cities_list=[], zoom=14, layer="basic", style="main"):
    """
    A function to initialize a clean TomTom map
    """

    maps_url = "http://{s}.api.tomtom.com/map/1/tile/" + layer + "/" + style + "/{z}/{x}/{y}.png?tileSize=512&key="
    TomTom_map = folium.Map(
        location=[-41.247,174.774],  # on what coordinates [lat, lon] initialise the map
        zoom_start=zoom,
        tiles=str(maps_url + api_key),
        attr='TomTom')

    for city in cities_list:
        folium.CircleMarker([city['lat'], city['lon']],
                            popup=city['name'],
                            radius=city['weight'],
                            color='b',
                            fill=True,
                            fill_opacity=0.7,
                            fill_color=city['color'],
                            ).add_to(TomTom_map)

    return TomTom_map

def heat_map_childs(api_key=api_key, in_df=None, zoom=14, layer="basic", style="main"):
    maps_url = "http://{s}.api.tomtom.com/map/1/tile/" + layer + "/" + style + "/{z}/{x}/{y}.png?tileSize=512&key="
    TomTom_map = folium.Map(
        location=[-41.247, 174.774],  # on what coordinates [lat, lon] initialise the map
        zoom_start=zoom,
        tiles=str(maps_url + api_key),
        attr='TomTom')
    total_cities = in_df['Txn_amt'].sum()
    in_df['percentage'] = (in_df['Txn_amt']/total_cities)*100

    # spend_pcts = state_grouped.groupby(level=0).apply(lambda x:
    #                                                  100 * x / float(x.sum()))
    grouped = in_df.groupby('city')


    for name, g in grouped:

        geo_id, position = SearchCity(name, 'New Zealand')
        folium.CircleMarker([position['lat'], position['lon']],
                            popup=name,
                            radius=g.reset_index().loc[0,'percentage'],
                            color='b',
                            fill=True,
                            fill_opacity=0.7,
                            fill_color='red',
                            ).add_to(TomTom_map)

    return TomTom_map
    # for name, group in state_grouped:
    #     st.write(name)
    #     state_marker_cluster = MarkerCluster(
    #             name=name,
    #             overlay=True,
    #             control=False,
    #             icon_create_function=None
    #         )
    #     spend_suburb = group.groupby(['suburb']).agg({'Txn_amt': 'sum'})
    #
    #     spend_pcts = spend_suburb.groupby(level=0).apply(lambda x:
    #                                                      100 * x / float(x.sum()))
    #     group['percentage'] = spend_pcts['Txn_amt']
    #
    #     # suburb_name = group.reset_index().loc[0, 'suburb']
    #     suburb_group = group.groupby(['suburb'])
    #     for suburb_name, tmp_group in suburb_group:
    #         geo_id, position = SearchCity(suburb_name, 'New Zealand')
    #         marker = folium.CircleMarker([position['lat'], position['lon']],
    #                             popup=suburb_name,
    #                             radius=tmp_group.reset_index().loc[0,'percentage'],
    #                             color='b',
    #                             fill=True,
    #                             fill_opacity=0.7,
    #                             fill_color='red',
    #                             )
    #         state_marker_cluster.add_child(marker)
    #
    #     # st.write(spend_pcts)
    #     state_marker_cluster.add_to(TomTom_map)
    # return TomTom_map

def add_marker_map(tomtom_map, marker_latitude, marker_longitude, marker_title='point', marker_color='green',
                   is_home=True):
    """
    A function to add a point marker at any TomTom map
    """
    if is_home:
        icon_ = 'glyphicon glyphicon-chevron-up'
    else:
        icon_ = 'glyphicon glyphicon-chevron-down'

    folium.Marker(location=(marker_latitude, marker_longitude),
                  popup=marker_title,
                  icon=folium.Icon(color=marker_color, icon=icon_)
                  ).add_to(tomtom_map)

    return tomtom_map

def get_cafes_around_point(dest_latitude, dest_longitude, search_limit, search_radius, category):
    """Function to retrieve cafes around given point"""

    # Gather all paramters in dict
    params_ = {'countrySet': 'NZ',
               'lat': dest_latitude,
               'lon': dest_longitude,
               'limit': search_limit,
               'radius': search_radius,
               'key': api_key
               }
    # create a request
    url = (f'https://api.tomtom.com/search/2/categorySearch/{category}.json')
    r = requests.get(url, params=params_)

    # check response status to make sure it went through
    if r.status_code == 200:
        result = r.json()
    return result

def SearchCity(city, country):
    """ Function use Geocoding feature in Search API to get lat/lon of the center of a city or area"""

    url = f'https://api.tomtom.com/search/2/search/{city},{country}.json?limit=1&idxSet=Geo&key={api_key}'
    result = requests.get(url).json()

    GeoID = result['results'][0]['dataSources']['geometry']['id']
    position = result['results'][0]['position']

    return GeoID, position

# @st.cache(suppress_st_warning=True)
def get_map_center_zoom(in_home_latitude, in_home_longitude, in_dest_latitude, in_dest_longitude):
    """Function to get center of folium map and zoom used"""
    # get center of our map based on home and destination points
    home_latitude = float(in_home_latitude)
    dest_latitude = float(in_dest_latitude)
    dest_longitude = float(in_dest_longitude)
    home_longitude = float(in_home_longitude)
    ave_lat = (home_latitude + dest_latitude) / 2
    ave_long = (home_longitude + dest_longitude) / 2

    # calculate map zoom
    zoom = 14  # defuelt zoom
    latitude_diff = abs(home_latitude - dest_latitude)
    longitude_diff = abs(home_longitude - dest_longitude)

    if latitude_diff > longitude_diff:
        while latitude_diff > 0:
            zoom = zoom - 1
            latitude_diff = latitude_diff - 0.1
    else:
        while longitude_diff > 0:
            zoom = zoom - 1
            longitude_diff = longitude_diff - 0.1
    return ave_lat, ave_long, zoom

# @st.cache(suppress_st_warning=True, hash_funcs={folium.Map: hash})
def draw_route(points):
    """Function to draw a route between two points using folium"""

    #     # get center of our map based on obtained route
    #     ave_lat = sum(float(p[0]) for p in points)/len(points)
    #     ave_long = sum(float(p[1]) for p in points)/len(points)

    lat_avg, long_avg, zoom_avg = get_map_center_zoom(float(points[0][0]), float(points[0][1]), float(points[-1][0]),
                                                      float(points[-1][1]))

    # initlize a map
    TomTom_map = init_map(api_key, latitude=lat_avg, longitude=long_avg, zoom=zoom_avg)

    # add a markers for home and destination points
    TomTom_map = add_marker_map(TomTom_map, points[0][0], points[0][1], marker_title='Your location',
                                marker_color='blue', is_home=True)
    TomTom_map = add_marker_map(TomTom_map, points[-1][0], points[-1][1], marker_title='Your destination point',
                                marker_color='red', is_home=False)

    # add lines
    folium.PolyLine(points, color="green", weight=2.5, opacity=1).add_to(TomTom_map)

    return TomTom_map

# @st.cache(suppress_st_warning=True, hash_funcs={folium.Map: hash})
def find_short_route(home_latitude, home_longitude, dest_latitude, dest_longitude):
    """Function to find the shortest route between 2 points"""
    url = "https://api.tomtom.com/routing/1/calculateRoute/{},{}:{},{}/xml?avoid=unpavedRoads&key={}".format(
        str(home_latitude).replace(' ',''),str(home_longitude).replace(' ',''),
        str(dest_latitude).replace(' ',''),str(dest_longitude).replace(' ',''),api_key
    )

    # Make the Request
    r = requests.get(url)

    # check response status to make sure it went through
    if r.status_code == 200:

        # Turn the XML data into a human readable format
        soup = BeautifulSoup(r.content, "lxml")

        # get Estimated travel time
        time_tot_sec = float(soup.find('summary').find('traveltimeinseconds').text)
        time_h = int(time_tot_sec / (60 * 60))
        time_m = int((time_tot_sec % (60 * 60)) / 60)

        # get distance
        distance = float(soup.find('summary').find('lengthinmeters').text) / 1000

        # get EAT
        eta = soup.find('summary').find('arrivaltime').text.split('T')
        eta_date = eta[0]
        eta_time = eta[1].split('+')[0]

        # Find all the tags that contain a point in our route
        points = soup.find_all('point')

        # clean points list
        points = [tuple([float(point['latitude']), float(point['longitude'])]) for point in points]

        # drow map
        tomtom_map = draw_route(points)

        return time_h, time_m, distance, eta_date, eta_time, tomtom_map, points
    else:
        return None,None,None,None,None,None,None

# def get_origin_destinations(in_df):
#     grouped = in_df.groupby(['Cust_num'])
#     tmp_lst = []
#     locator = Nominatim(user_agent='myGeocoder', timeout=5)
#     for name, group in grouped:
#         origin_cor = group.head(1).reset_index().loc[0,'Lat_long']
#         destination_cor = group.tail(1).reset_index().loc[0,'Lat_long']
#         if origin_cor is not np.nan:
#             location = locator.reverse(origin_cor)
#             # google_location = gmaps.reverse_geocode(origin_cor)
#             origin_city, origin_suburb, origin_road = get_address(location)
#         else:
#             origin_city, origin_suburb, origin_road = np.nan, np.nan, np.nan
#         if destination_cor is not np.nan:
#             location = locator.reverse(destination_cor)
#             destination_city, destination_suburb, destination_road = get_address(location)
#         else:
#             destination_city,destination_suburb, destination_road = np.nan, np.nan, np.nan
#         tmp_lst.append([name, origin_city, origin_suburb, origin_road, destination_city,
#                         destination_suburb, destination_road])
#     df_address = pd.DataFrame(tmp_lst,columns=['Cust_num','origin_city' ,'origin_suburb',
#                                                'origin_road','destination_city' ,'destination_suburb', 'destination_road'])
#     return df_address